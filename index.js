const xlsx = require('xlsx');
const fs = require('fs-extra');
const path = require('path');
const { exit } = require('process');

const excelFile = 'data.xlsx';
const filesDir = path.join(__dirname, 'files');

async function renameFiles() {
    // Load the workbook
    const workbook = xlsx.readFile(excelFile);
    const sheetName = workbook.SheetNames[0];
    const sheet = workbook.Sheets[sheetName];
    const objData = xlsx.utils.sheet_to_json(sheet);
    const data = objData.map(obj => obj[(Object.keys(obj))[0]]);

    // Read the directory files
    const files = await fs.readdir(filesDir);

    // Loop through the data and rename files
    let filesLength = files.length;
    for (let index = 1; index < filesLength; index++) {
        let oldFileName = files[index];
        let oldFileNumber = parseInt(oldFileName.substring(0, oldFileName.lastIndexOf('.')));
        if (!Number.isInteger(oldFileNumber)) {
            console.log(`Ignore ${oldFileName}, not a numbered file`);
            continue;
        }
        let newName = data[oldFileNumber - 1];
        if (!newName) {
            console.log(`Ignore ${oldFileName}, no new name data`);
            continue;
        }
        const oldPath = path.join(filesDir, oldFileName);
        const newPath = path.join(filesDir, newName + path.extname(oldFileName));
        if (await fs.pathExists(newPath)) {
            console.log(`Ignore ${oldFileName}, ${newName + path.extname(oldFileName)} exists`);
            continue;
        }
        await fs.rename(oldPath, newPath);
        console.log(`Renamed ${oldFileName} to ${newName + path.extname(oldFileName)}`);
    };
}

renameFiles().catch(err => { console.error(err); exit(); });